package br.com.agenda.exception;

public class AgendamentoException extends Exception{

	private static final long serialVersionUID = 1L;

	public AgendamentoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AgendamentoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public AgendamentoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public AgendamentoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public AgendamentoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
	

}
