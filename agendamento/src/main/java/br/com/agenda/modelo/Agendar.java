package br.com.agenda.modelo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import br.com.agenda.exception.AgendamentoException;

public class Agendar {
	
	
	public Consulta agenda(Paciente paciente, String dataTexto, List<Consulta> consultas) throws AgendamentoException{
		Date data = converterData(dataTexto);
		for (Consulta consulta : consultas) {
			if(consulta.getDataHoraConsulta().equals(data)){
				throw new AgendamentoException("N�o � poss�vel agendar consulta nesse hor�rio");
			}
		}
		return new Consulta(paciente, data);
	}
	
	public Date converterData(String datatexto){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:sss");
		Date data = null;
		try {
			data = sdf.parse(datatexto);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return data;
	}
	

}
