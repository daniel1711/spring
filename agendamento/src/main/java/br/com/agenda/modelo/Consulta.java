package br.com.agenda.modelo;

import java.util.Date;
import java.util.List;

public class Consulta {

	private Integer id;
	private Paciente paciente;
	private Date dataHoraConsulta;

	private  Consulta() {
		super();
		// TODO Auto-generated constructor stub
	}	

	public Integer getId() {
		return id;
	}

	public Consulta(Paciente paciente, Date dataHoraConsulta) {		
		this.paciente = paciente;
		this.dataHoraConsulta = dataHoraConsulta;
	}



	public void setId(Integer id) {
		this.id = id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Date getDataHoraConsulta() {
		return dataHoraConsulta;
	}

	public void setDataHoraConsulta(Date dataHoraConsulta) {
		this.dataHoraConsulta = dataHoraConsulta;
	}
	
	

}
