package br.com.agenda.modelo;

import java.util.Date;

public class Paciente {

	private Integer id;
	private String nome;

	private Documento documento = new Documento();
	private Endereco Endereco = new Endereco();

	private Date dataNascimento;

	public Paciente() {
	}
	
	public Paciente(String nome) {
		super();
		this.nome = nome;
	}



	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Documento getDocumento() {
		return documento;
	}

	public void setDocumento(Documento documento) {
		this.documento = documento;
	}

	public Endereco getEndereco() {
		return Endereco;
	}

	public void setEndereco(Endereco endereco) {
		Endereco = endereco;
	}

	public Date getDataNascimento() {
		return dataNascimento;
	}

	public void setDataNascimento(Date dataNascimento) {
		this.dataNascimento = dataNascimento;
	}

}
