package br.com.agendamento.modelo.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import br.com.agenda.exception.AgendamentoException;
import br.com.agenda.modelo.Agendar;
import br.com.agenda.modelo.Consulta;
import br.com.agenda.modelo.Paciente;

public class AgendaText {
	
	
	@Test (expected = AgendamentoException.class)
	public void naoDeveRealizarUmAgendamento() throws AgendamentoException{
		Paciente paciente1= new Paciente("Sergio Matos");
		Paciente paciente2 = new Paciente("Otavio Borg");
		List<Consulta> consultas = new ArrayList<Consulta>();
		
		Agendar agendar = new Agendar();
		
		Consulta consulta1 = agendar.agenda(paciente1, "24/08/2017 15:30:00", consultas);
		consultas.add(consulta1);		
		
		Consulta consulta2 = agendar.agenda(paciente2,"24/08/2017 15:30:00", consultas);
		consultas.add(consulta2);
		
		
	}


}
